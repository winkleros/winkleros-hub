{-# LANGUAGE OverloadedLabels  #-}
{-# LANGUAGE OverloadedStrings #-}
module Main where

import Data.GI.Base
--import Data.GI.Gtk.ComboBox (comboBoxSetModelText, comboBoxAppendText)
import qualified Data.Text as T
import GHC.Int
import qualified GI.Gtk as Gtk
import GI.Gtk.Objects.Adjustment
import System.Directory (copyFile, doesFileExist, getHomeDirectory, removeFile)
import System.IO
import System.Posix.User
import System.Process

main :: IO ()
main = do
  Gtk.init Nothing

  home <- getHomeDirectory
  user <- getEffectiveUserName
  let user2 = getEffectiveUserName
  let source = "/usr/share/applications/winkleros-hub.desktop"
  let dest = home ++ "/.config/autostart/winkleros-hub.desktop"
  let settings = home ++ "/.config/winkleros-hub/settings.conf"

  -- needed when creaing scrolledWindows
  let noAdjustment :: Maybe Adjustment
      noAdjustment = Nothing

  -- The myColors variable is the colorscheme we are using.
  let currentColors = do
        handle <- openFile settings ReadMode
        contents <- hGetContents handle
        let colscheme = drop 13 $ (lines contents) !! 1
        print colscheme
        hClose handle
        return colscheme
  myColors <- currentColors

  -- The myAutostart variable tells us if autostart is set to 'true' or 'false'.
  let autostartSetting = do
        handle <- openFile settings ReadMode
        contents <- hGetContents handle
        let locker = drop 11 $ (lines contents) !! 0
        print locker
        hClose handle
        return locker
  myAutostart <- autostartSetting

  css <- Gtk.cssProviderNew
  let cssFile = T.pack $ "/usr/share/winkleros-hub/css/" ++ myColors ++ ".css"
  Gtk.cssProviderLoadFromPath css cssFile

  win <- Gtk.windowNew Gtk.WindowTypeToplevel
  set win
    [ #borderWidth          := 10
    , #title                := T.pack "DTOS Hub"
    , #defaultWidth         := 920
    , #defaultHeight        := 480
    , #resizable            := True
    , #windowPosition       := Gtk.WindowPositionCenter
    , #decorated            := True
    ]

  aboutGrid <- Gtk.gridNew
  set aboutGrid
    [ #columnSpacing        := 10
    , #rowSpacing           := 10
    , #columnHomogeneous    := True
    ]

  aboutImage <- Gtk.imageNewFromFile "/usr/share/winkleros-hub/img/winkleros-logo.png"

  aboutLabel1 <- Gtk.labelNew $ Just "Welcome to DTOS! Need help using DTOS or customizing it?"
  aboutLabel2 <- Gtk.labelNew $ Just "Or maybe you just want to learn more about Linux? We've got you covered."
  aboutLabel3 <- Gtk.labelNew $ Just "You can help support DT's work including his work on DTOS."
  aboutLabel4 <- Gtk.labelNew Nothing
  Gtk.labelSetMarkup aboutLabel4 "Please subscribe to <b>DistroTube</b> on <b>Patreon</b>."

  aboutEmpty1 <- Gtk.labelNew Nothing
  aboutEmpty2 <- Gtk.labelNew Nothing
  aboutEmpty3 <- Gtk.labelNew Nothing
  aboutEmpty4 <- Gtk.labelNew Nothing

  softBtn <- Gtk.buttonNewWithLabel "Software Center"
  on softBtn #clicked $ do
    putStrLn "User chose: Software Center"
    callCommand "/usr/bin/octopi &"

  screenBtn <- Gtk.buttonNewWithLabel "Screen Resolution"
  on screenBtn #clicked $ do
    putStrLn "User chose: Screen Resolution"
    callCommand "arandr &"

  lookFeelBtn <- Gtk.buttonNewWithLabel "Look & Feel"
  on lookFeelBtn #clicked $ do
    putStrLn "User chose: Look & Feel"
    callCommand "lxappearance &"

  aboutBtn <- Gtk.buttonNewWithLabel "About DTOS"
  on aboutBtn #clicked $ do
    putStrLn "User chose: Learn More"
    callCommand "xdg-open https://winkleros.dev &"

  releaseBtn <- Gtk.buttonNewWithLabel "Release Notes"
  on releaseBtn #clicked $ do
    putStrLn "User chose: Release Notes"
    callCommand "xdg-open https://winkleros.dev &"

  faqBtn <- Gtk.buttonNewWithLabel "FAQ"
  on faqBtn #clicked $ do
    putStrLn "User chose: FAQ"
    callCommand "xdg-open https://winkleros.dev &"

  getInvBtn <- Gtk.buttonNewWithLabel "Get Involved"
  on getInvBtn #clicked $ do
    putStrLn "User chose: Get Involved"
    callCommand "xdg-open https://winkleros.dev &"

  forumBtn <- Gtk.buttonNewWithLabel "Forums"
  on forumBtn #clicked $ do
    putStrLn "User chose: Forums"
    callCommand "xdg-open https://winkleros.dev &"

  donateBtn <- Gtk.buttonNewWithLabel "Donate on Patreon"
  on donateBtn #clicked $ do
    putStrLn "User chose: Donate on Patreon"
    callCommand "xdg-open https://winkleros.dev &"

  youTubeBtn <- Gtk.buttonNewWithLabel "YouTube"
  on youTubeBtn #clicked $ do
    putStrLn "User chose: YouTube"
    callCommand "xdg-open https://winkleros.dev &"

  exitBtn <- Gtk.buttonNewWithLabel "Exit"
  on exitBtn #clicked $ do
    putStrLn "User chose: Exit"
    Gtk.widgetDestroy win

  ------------------------------------------------------
  -- The checkButton with the autostart functionality --
  ------------------------------------------------------
  check <- Gtk.checkButtonNewWithLabel "Autostart"
  if myAutostart == "true"
     then do
         Gtk.toggleButtonSetActive check True
     else do
         Gtk.toggleButtonSetActive check False

  on check #clicked $ do
    state <- Gtk.toggleButtonGetActive check
    if state
       then do putStrLn "Autostart set to TRUE"
               writeFile settings $ "autostart: true\ncolorscheme: " ++ myColors
               copyFile source dest
       else do putStrLn "Autostart set to FALSE"
               writeFile settings $ "autostart: false\ncolorscheme: " ++ myColors
               -- Check if .desktop is already in autostart dir.
               fileExists <- doesFileExist dest
               if fileExists then removeFile dest else putStrLn "No need."

  #add win aboutGrid

  -- plack everything on the grid
  #attach aboutGrid aboutEmpty1  0 0 11 1
  #attach aboutGrid aboutImage   1 1 9 2
  #attach aboutGrid aboutEmpty2  0 3 11 1
  #attach aboutGrid aboutLabel1  1 4 9 1
  #attach aboutGrid aboutLabel2  1 5 9 1
  #attach aboutGrid aboutLabel3  1 6 9 1
  #attach aboutGrid aboutLabel4  1 8 9 1
  #attach aboutGrid aboutEmpty3  0 9 11 1
  #attach aboutGrid softBtn      1 10 3 1
  #attach aboutGrid screenBtn    4 10 3 1
  #attach aboutGrid lookFeelBtn  7 10 3 1
  #attach aboutGrid aboutBtn     1 11 2 1
  #attach aboutGrid releaseBtn   3 11 2 1
  #attach aboutGrid faqBtn       5 11 1 1
  #attach aboutGrid getInvBtn    6 11 2 1
  #attach aboutGrid forumBtn     8 11 2 1
  #attach aboutGrid donateBtn    1 12 3 1
  #attach aboutGrid youTubeBtn   4 12 3 1
  #attach aboutGrid exitBtn      7 12 3 1
  #attach aboutGrid aboutEmpty4  0 13 11 1
  #attach aboutGrid check        8 14 3 1

  Gtk.onWidgetDestroy win Gtk.mainQuit
  #showAll win
  Gtk.main
